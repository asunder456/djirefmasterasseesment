FROM python:3

ADD *.py /

RUN pip install pystrich

CMD [ "python", "./mainscript.py" ]
